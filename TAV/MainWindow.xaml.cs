﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace TAV
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static int userRole;
        public MainWindow()
        {
            InitializeComponent();
            double x = System.Windows.SystemParameters.PrimaryScreenWidth;
            double y = System.Windows.SystemParameters.PrimaryScreenHeight;
            Height = y * 0.8;
            Width = x * 0.8;
        }

        private void ButtonFechar_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void ListViewMenu_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = ListViewMenu.SelectedIndex;
            MoveCursorMenu(index);
            switch (index)
            {
                case 0:
                    GridPrincipal.Children.Clear();
                    break;

                case 1:
                    if (userRole == 1)
                    {
                        GridPrincipal.Children.Clear();
                        GridPrincipal.Children.Add(new EnginePanel());
                    }
                    else
                    {
                        EnginePanel enginePanel = new EnginePanel();
                        enginePanel.add_button.IsEnabled = false;
                        enginePanel.delete_button.IsEnabled = false;
                        enginePanel.edit_button.IsEnabled = false;
                        GridPrincipal.Children.Clear();
                        GridPrincipal.Children.Add(enginePanel);
                    }


                    break;

                case 2:

                    if (userRole == 1)
                    {
                        GridPrincipal.Children.Clear();
                        GridPrincipal.Children.Add(new ResourcesPanel());
                    }
                    else
                    {
                        ResourcesPanel resourcesPanel = new ResourcesPanel();
                        resourcesPanel.add_button.IsEnabled = false;
                        resourcesPanel.delete_button.IsEnabled = false;
                        resourcesPanel.edit_button.IsEnabled = false;
                        GridPrincipal.Children.Clear();
                        GridPrincipal.Children.Add(resourcesPanel);
                    }

                    break;

                default:
                    GridPrincipal.Children.Clear();
                    break;
            }
        }

        private void MoveCursorMenu(int index)
        {
            TrainsitionigContentSlide.OnApplyTemplate();
            GridCursor.Margin = new Thickness(0, (100 + (60 * index)), 0, 0);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://gitlab.com/lord007tn/tav-management");
        }

        private void ButtonMinimize_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }
    }
}