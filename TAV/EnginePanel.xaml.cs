﻿using MySql.Data.MySqlClient;
using System;
using System.Configuration;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using TAV.utils;

namespace TAV
{
    /// <summary>
    /// Interaction logic for EnginePanel.xaml
    /// </summary>
    ///
    public partial class EnginePanel : UserControl
    {
        public static int userRole;
        private DataTable dt;
        private MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

        public EnginePanel()
        {
            InitializeComponent();
            LoadEngineData();
        }

        public void LoadEngineData()
        {
            if (con.State != ConnectionState.Open) { con.Open(); }

            try
            {
                string query = "select * from engine e inner join engine_details as ed on ed.engine_idEngines = e.idEngines";
                MySqlCommand cmd = new MySqlCommand(query, con);
                cmd.Connection = con;
                dt = new DataTable();
                MySqlDataAdapter sda = new MySqlDataAdapter(cmd);
                sda.Fill(dt);

                DataColumn dc = new DataColumn("ischecked");
                dc.DataType = typeof(bool);
                dc.AllowDBNull = false;
                dc.DefaultValue = false;
                dt.Columns.Add(dc);
                enginetable.ItemsSource = dt.DefaultView;
                con.Close();
            }
            catch (Exception ex)
            {
                VerPopup(ex.Message, "OK");
            }
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (CheckBox.IsChecked.Value == true)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    dr["ischecked"] = true;
                }
            }
            else
            {
                foreach (DataRow dr in dt.Rows)
                {
                    dr["ischecked"] = false;
                }

            }
        }

        private void refresh_button_Click(object sender, RoutedEventArgs e)
        {
            LoadEngineData();
        }

        private void delete_button_Click(object sender, RoutedEventArgs e)
        {
            Popup1 popup = new Popup1();

            popup.pop_text.Text = " Want to delete?";
            popup.pop_confirm_button = new Button { Content = "Delete" };
            popup.pop_cancel_button = new Button { Content = "Cancel" };
            popup.Height = 200;
            popup.Width = 400;
            popup.WindowStartupLocation = WindowStartupLocation.CenterScreen;


            if (con.State != ConnectionState.Open) { con.Open(); }
            foreach (DataRow dr in dt.Rows)
            {
                if (Convert.ToBoolean(dr["ischecked"]))
                {
                    popup.ShowDialog();
                    if (popup.DialogResult == true)
                    {
                        try
                        {
                            string query = "delete from engine_details ed where( id_details = " + dr["id_details"] + ");";
                            MySqlCommand cmd = new MySqlCommand(query, con);
                            cmd.Connection = con;
                            dt = new DataTable();
                            MySqlDataAdapter sda = new MySqlDataAdapter(cmd);
                            sda.Fill(dt);
                            LoadEngineData();
                            CheckBox.IsChecked = false;
                        }
                        catch (Exception ex)
                        {
                            VerPopup(ex.Message, "OK");
                        }
                    }
                }
            }
        }

        private void DataDetailsToUpdateFill(out DateTime started_time, out DateTime stopped_time, out string clarification, out int idengine)
        {
            started_time = DateTime.Now; stopped_time = DateTime.Now; clarification = "0"; idengine = -1;
            if (CheckCount())
            {
                foreach (DataRow dr in dt.Rows)
                    if (Convert.ToBoolean(dr["ischecked"]))
                    {
                        {
                            started_time = (DateTime)dr["started_time"];
                            stopped_time = (DateTime)dr["stopped_time"];
                            clarification = (string)dr["clarifications"];
                            idengine = (int)dr["engine_idEngines"];
                        }
                    }
            }
        }

        private string GetEngineName(int engineid)
        {
            if (con.State != ConnectionState.Open) { con.Open(); }
            try
            {
                string query = "select engine_name from engine where( idEngines = " + engineid + ");";
                MySqlCommand cmd = new MySqlCommand(query, con);
                cmd.Connection = con;
                string engine_name = (string)cmd.ExecuteScalar();
                con.Close();
                return engine_name;
            }
            catch (Exception ex)
            {
                VerPopup(ex.Message, "OK");
                return null;
            }
        }

        private void edit_button_Click(object sender, RoutedEventArgs e)
        {
            DateTime started_time, stopped_time;
            string clarification;
            int idengine;
            if (CheckCount())
            {
                foreach (DataRow dr in dt.Rows)
                {
                    if (Convert.ToBoolean(dr["ischecked"]))
                    {
                        DataDetailsToUpdateFill(out started_time, out stopped_time, out clarification, out idengine);
                        EditWindow Ewindow = new EditWindow();
                        Ewindow.Height = 380;
                        Ewindow.Width = 400;
                        Ewindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                        Ewindow.start_time.Text = started_time.ToString("HH:mm:ss");
                        Ewindow.stop_time.Text = stopped_time.ToString("HH:mm:ss");
                        Ewindow.start_date.Text = started_time.ToString("yyyy/MM/dd");
                        Ewindow.stop_date.Text = stopped_time.ToString("yyyy/MM/dd");
                        Ewindow.clarifications_box.Text = clarification;
                        string engine_name = GetEngineName(idengine);
                        Ewindow.engine_combobox.Text = engine_name;
                        Ewindow.Iddetail = (int)dr["id_details"];
                        Ewindow.ShowDialog();
                    }
                }
            }
            else
            {
                VerPopup("Check one item only", "OK");
            }
        }

        private void search_box_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (con.State != ConnectionState.Open) { con.Open(); }
            try
            {
                string query = "select *  from engine e, engine_details ed where( engine_name like '%" + search_box.Text + "%' and ed.engine_idEngines = e.idEngines);";
                MySqlCommand cmd = new MySqlCommand(query, con);
                cmd.Connection = con;
                dt = new DataTable();
                MySqlDataAdapter sda = new MySqlDataAdapter(cmd);
                sda.Fill(dt);
                DataColumn dc = new DataColumn("ischecked");
                dc.DataType = typeof(bool);
                dc.AllowDBNull = false;
                dc.DefaultValue = false;
                dt.Columns.Add(dc);
                enginetable.ItemsSource = dt.DefaultView;
                CheckBox.IsChecked = false;
                con.Close();
            }
            catch (Exception ex)
            {
                VerPopup(ex.Message, "OK");
            }
        }

        private void add_button_Click(object sender, RoutedEventArgs e)
        {
            AddWindow Awindow = new AddWindow();
            Awindow.Height = 400;
            Awindow.Width = 400;
            Awindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            Awindow.ShowDialog();
        }

        private void VerPopup(string ptext, string bcontent)
        {
            VerificationPopup verpop = new VerificationPopup();
            verpop.ver_text.Text = ptext;
            verpop.ver_confirm_button = new Button { Content = bcontent };
            verpop.MinHeight = 300;
            verpop.MinWidth = 400;
            verpop.ver_text.TextWrapping = TextWrapping.Wrap;
            verpop.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            verpop.ShowDialog();
        }

        private bool CheckCount()
        {
            int count = 0;
            foreach (DataRow dr in dt.Rows)
            {
                if (Convert.ToBoolean(dr["ischecked"]) == true) { count++; }
            }
            if (count != 1) { return false; }
            else { return true; }
        }

        private void add_engine_button_Click(object sender, RoutedEventArgs e)
        {
            AddEngines Ewindow = new AddEngines();
            Ewindow.Height = 200;
            Ewindow.Width = 400;
            Ewindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            Ewindow.ShowDialog();
        }
    }
}