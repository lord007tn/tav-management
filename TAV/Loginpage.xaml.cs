﻿using MySql.Data.MySqlClient;
using System;
using System.Configuration;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using TAV.utils;

namespace TAV
{
    /// <summary>
    /// Interaction logic for Loginpage.xaml
    /// </summary>
    public partial class Loginpage : Window
    {
        public Loginpage()
        {
            InitializeComponent();
        }

        private void VerPopup(string ptext, string bcontent)
        {
            VerificationPopup verpop = new VerificationPopup();
            verpop.ver_text.Text = ptext;
            verpop.ver_confirm_button = new Button { Content = bcontent };
            verpop.MinHeight = 300;
            verpop.MinWidth = 400;
            verpop.ver_text.TextWrapping = TextWrapping.Wrap;
            verpop.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            verpop.ShowDialog();
        }

        private void ButtonLogin_Click(object sender, RoutedEventArgs e)
        {
            MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

            if (con.State != ConnectionState.Open) { con.Open(); }

            try
            {
                if (username.Text != "" && password.Password != "")
                {
                    string query = "Select * from tavdb.user where username=@username AND password=@password limit 1";

                    MySqlCommand cmd = new MySqlCommand(query, con);
                    cmd.Parameters.AddWithValue("@username", username.Text);
                    cmd.Parameters.AddWithValue("@password", password.Password);
                    cmd.Connection = con;

                    DataTable dt = new DataTable();
                    MySqlDataAdapter sda = new MySqlDataAdapter(cmd);
                    sda.Fill(dt);
                    int role =Convert.ToInt32(dt.Rows[0]["permession_type"]);

                    if (dt.Rows.Count <= 0)
                    {
                        VerPopup("Username/Password is invalid", "OK");
                    }
                    else
                    {
                        MainWindow mainWindow = new MainWindow();
                        MainWindow.userRole = role;
                        EnginePanel.userRole = role;
                        ResourcesPanel.userRole = role;

                        if (role == 1)
                        {
                            
                            this.Close();
                            mainWindow.ShowDialog();
                        }
                        else
                        {
                            mainWindow.Clients.IsEnabled = false;
                            this.Close();
                            mainWindow.ShowDialog();
                        }

                    }
                }
                else
                {
                    VerPopup("Username or Password is empty", "OK");
                }
            }
            catch
            {
                VerPopup("Username/Password is invalid", "OK");
            }
        }
    }
}