﻿using MySql.Data.MySqlClient;
using System;
using System.Configuration;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Effects;

namespace TAV.utils
{
    /// <summary>
    /// Interaction logic for ResourceAddWindow.xaml
    /// </summary>
    public partial class ResourceAddWindow : Window
    {
        private MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        private DataTable dt;

        public ResourceAddWindow()
        {
            InitializeComponent();
            LoadComboBoxesData();
        }

        private void LoadComboBoxesData()
        {
            unit_combobox.SelectedItem = "Electric";
            unit_combobox.Items.Add("Electric");
            unit_combobox.Items.Add("AGL");
            unit_combobox.Items.Add("Trigen");

            degree_combobox.SelectedItem = "Normal";
            degree_combobox.Items.Add("High");
            degree_combobox.Items.Add("Normal");
            degree_combobox.Items.Add("Low");

            status_combobox.SelectedItem = "Pending";
            status_combobox.Items.Add("Approved");
            status_combobox.Items.Add("Pending");
            status_combobox.Items.Add("Rejected");

            payment_status_combobox.SelectedItem = "Unpaid";
            payment_status_combobox.Items.Add("Paid");
            payment_status_combobox.Items.Add("Unpaid");

            quantity_box.Value = 0;
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void VerPopup(string ptext, string bcontent)
        {
            VerificationPopup verpop = new VerificationPopup();
            verpop.ver_text.Text = ptext;
            verpop.ver_confirm_button = new Button { Content = bcontent };
            verpop.MinHeight = 300;
            verpop.MinWidth = 400;
            verpop.ver_text.TextWrapping = TextWrapping.Wrap;
            verpop.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            verpop.ShowDialog();
        }

        private void add_button_Click(object sender, RoutedEventArgs e)
        {
            if (con.State != ConnectionState.Open) { con.Open(); }
            if (resource_box.Text == "" | justification_box.Text == "" | designation_box.Text == "" | approved_date.Text == "" | MR_number_box.Text == "" | PO_number_box.Text == "" | created_by_box.Text == "" | notification_box.Text == "")
            {
                VerPopup("You need to fill all data field", "OK");
            }
            else
            {
                try
                {
                    DateTime ad = DateTime.Parse(approved_date.Text);
                    string ads = ad.ToString("yyyy-MM-dd HH:mm:ss");

                    string query = "insert into resources(item_name, quantity, unit, designation, justification, degree, status, approved_date, MR_number, po_number, payment_status, po_created_by, notification )" +
                        " values('" + resource_box.Text + "', " + quantity_box.Value + ", '" + unit_combobox.SelectedValue + "','" + designation_box.Text + "' , '" + justification_box.Text + "', '" + degree_combobox.SelectedValue + "', '" + status_combobox.SelectedValue + "', '" + ads + "', '" + MR_number_box.Text + "', '" + PO_number_box.Text + "', '" + payment_status_combobox.SelectedValue + "', '" + created_by_box.Text + "', '" + notification_box.Text + "'); ";
                    MySqlCommand cmd = new MySqlCommand(query, con);
                    cmd.Connection = con;
                    dt = new DataTable();
                    MySqlDataAdapter sda = new MySqlDataAdapter(cmd);
                    sda.Fill(dt);
                    Emptify();
                    BlurEffect blur = new BlurEffect();
                    blur.Radius = 5;
                    this.Effect = blur;
                    VerPopup("You have successfully add a Resource", "OK");
                    con.Close();
                    this.Effect = null;
                }
                catch (Exception ex)
                {
                    VerPopup(ex.Message, "OK");
                }
            }
        }

        private void cancel_button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Emptify()
        {
            unit_combobox.SelectedItem = "Electric";
            degree_combobox.SelectedItem = "Normal";
            status_combobox.SelectedItem = "Pending";
            payment_status_combobox.SelectedItem = "Unpaid";
            resource_box.Text = string.Empty;
            justification_box.Text = string.Empty;
            designation_box.Text = string.Empty;
            quantity_box.Value = 0;
            approved_date.Text = string.Empty;
            MR_number_box.Text = string.Empty;
            PO_number_box.Text = string.Empty;
            created_by_box.Text = string.Empty;
            notification_box.Text = string.Empty;
        }
    }
}