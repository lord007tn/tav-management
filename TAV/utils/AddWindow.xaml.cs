﻿using MySql.Data.MySqlClient;
using System;
using System.Configuration;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Effects;

namespace TAV.utils
{
    /// <summary>
    /// Interaction logic for AddWindow.xaml
    /// </summary>
    public partial class AddWindow : Window
    {
        private MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        private DataTable dt;

        public AddWindow()
        {
            InitializeComponent();
            EngineComboBoxFill();
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void cancel_button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void VerPopup(string ptext, string bcontent)
        {
            VerificationPopup verpop = new VerificationPopup();
            verpop.ver_text.Text = ptext;
            verpop.ver_confirm_button = new Button { Content = bcontent };
            verpop.MinHeight = 300;
            verpop.MinWidth = 400;
            verpop.ver_text.TextWrapping = TextWrapping.Wrap;
            verpop.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            verpop.ShowDialog();
        }

        private void EngineComboBoxFill()
        {
            if (con.State != ConnectionState.Open) { con.Open(); }
            try
            {
                string query = "select engine_name from engine";
                MySqlCommand cmd = new MySqlCommand(query, con);
                cmd.Connection = con;
                DataTable dt = new DataTable("engine_name");
                MySqlDataAdapter sda = new MySqlDataAdapter(cmd);
                sda.Fill(dt);
                engine_combobox.ItemsSource = dt.DefaultView;
                engine_combobox.DisplayMemberPath = "engine_name";
                engine_combobox.SelectedValuePath = "engine_name";
                con.Close();
            }
            catch (Exception ex)
            {
                VerPopup(ex.Message, "OK");
            }
        }

        private void add_button_Click(object sender, RoutedEventArgs e)
        {
            if (con.State != ConnectionState.Open) { con.Open(); }

            if (engine_combobox.SelectedItem == null | stop_time.Text == "" | stop_date.Text == "" | start_time.Text == "" | start_date.Text == "" | clarifications_box.Text == "")
            {
                VerPopup("You need to fill all data field", "OK");
            }
            else
            {
                try
                {
                    string startTime = start_date.Text + " " + start_time.Text;
                    string stopTime = stop_date.Text + " " + stop_time.Text;
                    DateTime st = DateTime.Parse(startTime);
                    string stt = st.ToString("yyyy-MM-dd HH:mm:ss");
                    DateTime pt = DateTime.Parse(stopTime);
                    string ptt = pt.ToString("yyyy-MM-dd HH:mm:ss");
                    TimeSpan stopDuration = st - pt;

                    int idEngines = GetidEngines();
                    string query = "insert into engine_details(started_time,stopped_time, stop_duration, clarifications, engine_idEngines) values('" + stt + "', '" + ptt + "', '" + stopDuration + "','" + clarifications_box.Text + "' , " + idEngines + "); ";
                    MySqlCommand cmd = new MySqlCommand(query, con);
                    cmd.Connection = con;
                    dt = new DataTable();
                    MySqlDataAdapter sda = new MySqlDataAdapter(cmd);
                    sda.Fill(dt);
                    Emptify();
                    BlurEffect blur = new BlurEffect();
                    blur.Radius = 5;
                    this.Effect = blur;
                    VerPopup("You have successfully add an Engine detail", "OK");
                    con.Close();
                    this.Effect = null;
                }
                catch (Exception ex)
                {
                    VerPopup(ex.Message, "OK");
                }
            }
        }

        private int GetidEngines()
        {
            try
            {
                if (con.State != ConnectionState.Open) { con.Open(); }
                string query = "select idEngines from engine where( engine_name = '" + engine_combobox.SelectedValue + "') limit 1;";
                MySqlCommand cmd = new MySqlCommand(query, con);
                cmd.Connection = con;
                int idEngines = (Int32)cmd.ExecuteScalar();
                con.Close();
                return idEngines;
            }
            catch (Exception ex)
            {
                VerPopup(ex.Message, "OK");
            }

            return -1;
        }

        private void Emptify()
        {
            engine_combobox.SelectedItem = null;
            stop_time.Text = string.Empty;
            stop_date.Text = string.Empty;
            start_time.Text = string.Empty;
            start_date.Text = string.Empty;
            clarifications_box.Text = string.Empty;
        }
    }
}