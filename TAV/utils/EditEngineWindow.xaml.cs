﻿using MySql.Data.MySqlClient;
using System.Configuration;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Effects;

namespace TAV.utils
{
    /// <summary>
    /// Interaction logic for EditEngineWindow.xaml
    /// </summary>
    public partial class EditEngineWindow : Window
    {
        private MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        private DataTable dt;

        public EditEngineWindow()
        {
            InitializeComponent();
        }

        private void engine_update_button_Click(object sender, RoutedEventArgs e)
        {
            if (con.State != ConnectionState.Open) { con.Open(); }
            string query = "update engine set engine_name='" + engine_box.Text + " where idEngines = ;";
            MySqlCommand cmd = new MySqlCommand(query, con);
            cmd.Connection = con;
            dt = new DataTable();
            MySqlDataAdapter sda = new MySqlDataAdapter(cmd);
            sda.Fill(dt);
            engine_box.Text = string.Empty;
            BlurEffect blur = new BlurEffect();
            blur.Radius = 5;
            this.Effect = blur;
            VerPopup("You have successfully Edit an Engine name", "OK");
            con.Close();
            this.Effect = null;
        }

        private void VerPopup(string ptext, string bcontent)
        {
            VerificationPopup verpop = new VerificationPopup();
            verpop.ver_text.Text = ptext;
            verpop.ver_confirm_button = new Button { Content = bcontent };
            verpop.MinHeight = 300;
            verpop.MinWidth = 400;
            verpop.ver_text.TextWrapping = TextWrapping.Wrap;
            verpop.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            verpop.ShowDialog();
        }


        private void cancel_button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }
    }
}