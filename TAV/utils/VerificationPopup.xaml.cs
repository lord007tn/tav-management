﻿using System.Windows;
using System.Windows.Input;

namespace TAV.utils
{
    /// <summary>
    /// Interaction logic for Popup1.xaml
    /// </summary>
    public partial class VerificationPopup : Window
    {
        public VerificationPopup()
        {
            InitializeComponent();
        }

        private void Ver_ok_button_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
    }
}