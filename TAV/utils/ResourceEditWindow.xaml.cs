﻿using MySql.Data.MySqlClient;
using System;
using System.Configuration;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Effects;

namespace TAV.utils
{
    /// <summary>
    /// Interaction logic for ResourceAddWindow.xaml
    /// </summary>
    public partial class ResourceEditWindow : Window
    {
        private MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        private DataTable dt;
        private int resources_id;

        public int Resources_id { get => resources_id; set => resources_id = value; }

        public ResourceEditWindow()
        {
            InitializeComponent();
            LoadComboBoxesData();
        }

        private void LoadComboBoxesData()
        {
            unit_combobox.SelectedItem = "Electric";
            unit_combobox.Items.Add("Electric");
            unit_combobox.Items.Add("AGL");
            unit_combobox.Items.Add("Trigen");

            degree_combobox.SelectedItem = "Normal";
            degree_combobox.Items.Add("High");
            degree_combobox.Items.Add("Normal");
            degree_combobox.Items.Add("Low");

            status_combobox.SelectedItem = "Pending";
            status_combobox.Items.Add("Approved");
            status_combobox.Items.Add("Pending");
            status_combobox.Items.Add("Rejected");

            payment_status_combobox.SelectedItem = "Unpaid";
            payment_status_combobox.Items.Add("Paid");
            payment_status_combobox.Items.Add("Unpaid");

            quantity_box.Value = 0;
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void VerPopup(string ptext, string bcontent)
        {
            VerificationPopup verpop = new VerificationPopup();
            verpop.ver_text.Text = ptext;
            verpop.ver_confirm_button = new Button { Content = bcontent };
            verpop.MinHeight = 300;
            verpop.MinWidth = 400;
            verpop.ver_text.TextWrapping = TextWrapping.Wrap;
            verpop.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            verpop.ShowDialog();
        }

        private void edit_button_Click(object sender, RoutedEventArgs e)
        {
            if (con.State != ConnectionState.Open) { con.Open(); }
            try
            {
                DateTime ad = DateTime.Parse(approved_date.Text);
                string ads = ad.ToString("yyyy-MM-dd HH:mm:ss");

                string query = "update resources set item_name='" + resource_box.Text + "', quantity=" + quantity_box.Value + " , unit='" + unit_combobox.SelectedValue + "' , designation='" + designation_box.Text + "', justification='" + justification_box.Text + "', degree='" + degree_combobox.SelectedValue + "', status='" + status_combobox.SelectedValue + "', approved_date='" + ads + "', MR_number= " + MR_number_box.Text + ", po_number=" + PO_number_box.Text + ", payment_status='" + payment_status_combobox.SelectedValue + "', po_created_by='" + created_by_box.Text + "', notification='" + notification_box.Text + "' " +
                    " where resources_id = " + Resources_id + "";
                MySqlCommand cmd = new MySqlCommand(query, con);
                cmd.Connection = con;
                dt = new DataTable();
                MySqlDataAdapter sda = new MySqlDataAdapter(cmd);
                sda.Fill(dt);
                BlurEffect blur = new BlurEffect();
                blur.Radius = 5;
                this.Effect = blur;
                VerPopup("You have successfully add a Resource", "OK");
                con.Close();
                this.Effect = null;
            }
            catch (Exception ex)
            {
                VerPopup(ex.Message, "OK");
            }
        }

        private void cancel_button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}