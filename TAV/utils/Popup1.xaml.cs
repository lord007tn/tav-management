﻿using System.Windows;

namespace TAV.utils
{
    /// <summary>
    /// Interaction logic for Popup1.xaml
    /// </summary>
    public partial class Popup1 : Window
    {
        public Popup1()
        {
            InitializeComponent();
        }

        private void Pop_confirm_button_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void Pop_cancel_button_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}