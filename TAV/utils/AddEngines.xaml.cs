﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TAV.utils
{
    /// <summary>
    /// Interaction logic for AddEngines.xaml
    /// </summary>
    public partial class AddEngines : Window
    {

        private MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);
        private DataTable dt;
        public AddEngines()
        {
            InitializeComponent();
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                DragMove();
            }
        }

        private void add_button_Click(object sender, RoutedEventArgs e)
        {
            string ptext = "You have successfully add an Engine";
            string bcontent = "OK";

            if (con.State != ConnectionState.Open) { con.Open(); }
            try
            {
                if (engine_box.Text == "")
                {
                    VerPopup("Invalid engine name ", "OK");
                }
                else
                {
                    string query = "insert into engine(engine_name) values('" + engine_box.Text + "');";
                    MySqlCommand cmd = new MySqlCommand(query, con);
                    cmd.Connection = con;
                    dt = new DataTable();
                    MySqlDataAdapter sda = new MySqlDataAdapter(cmd);
                    sda.Fill(dt);
                    engine_box.Text = string.Empty;
                    BlurEffect blur = new BlurEffect();
                    blur.Radius = 5;
                    this.Effect = blur;
                    VerPopup(ptext, bcontent);
                    this.Effect = null;
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                VerPopup(ex.Message, "OK");
            }
        }

        private void cancel_button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void VerPopup(string ptext, string bcontent)
        {
            VerificationPopup verpop = new VerificationPopup();
            verpop.ver_text.Text = ptext;
            verpop.ver_confirm_button = new Button { Content = bcontent };
            verpop.MinHeight = 300;
            verpop.MinWidth = 400;
            verpop.ver_text.TextWrapping = TextWrapping.Wrap;
            verpop.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            verpop.ShowDialog();
        }
    }
}
