﻿using MySql.Data.MySqlClient;
using System;
using System.Configuration;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using TAV.utils;

namespace TAV
{
    /// <summary>
    /// Interaction logic for ResourcesPanel.xaml
    /// </summary>
    public partial class ResourcesPanel : UserControl
    {
        public static int userRole;
        private DataTable dt;
        private MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString);

        public ResourcesPanel()
        {
            InitializeComponent();
            LoadResourcesData();
        }

        private void VerPopup(string ptext, string bcontent)
        {
            VerificationPopup verpop = new VerificationPopup();
            verpop.ver_text.Text = ptext;
            verpop.ver_confirm_button = new Button { Content = bcontent };
            verpop.MinHeight = 300;
            verpop.MinWidth = 400;
            verpop.ver_text.TextWrapping = TextWrapping.Wrap;
            verpop.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            verpop.ShowDialog();
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (CheckBox.IsChecked.Value == true)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    dr["ischecked"] = true;
                }
            }
            else
            {
                foreach (DataRow dr in dt.Rows)
                {
                    dr["ischecked"] = false;
                }
            }
        }

        private void refresh_button_Click(object sender, RoutedEventArgs e)
        {
            LoadResourcesData();
        }

        public void LoadResourcesData()
        {
            if (con.State != ConnectionState.Open) { con.Open(); }

            try
            {
                string query = "select * from resources";
                MySqlCommand cmd = new MySqlCommand(query, con);
                cmd.Connection = con;
                dt = new DataTable();
                MySqlDataAdapter sda = new MySqlDataAdapter(cmd);
                sda.Fill(dt);

                DataColumn dc = new DataColumn("ischecked");
                dc.DataType = typeof(bool);
                dc.AllowDBNull = false;
                dc.DefaultValue = false;
                dt.Columns.Add(dc);
                resourcestable.ItemsSource = dt.DefaultView;
                con.Close();
            }
            catch (Exception ex)
            {
                VerPopup(ex.Message, "OK");
            }
        }

        private void delete_button_Click(object sender, RoutedEventArgs e)
        {
            Popup1 popup = new Popup1();

            popup.pop_text.Text = " Want to delete?";
            popup.pop_confirm_button = new Button { Content = "Delete" };
            popup.pop_cancel_button = new Button { Content = "Cancel" };
            popup.Height = 200;
            popup.Width = 400;
            popup.WindowStartupLocation = WindowStartupLocation.CenterScreen;


            if (con.State != ConnectionState.Open) { con.Open(); }
            foreach (DataRow dr in dt.Rows)
            {
                if (Convert.ToBoolean(dr["ischecked"]))
                {
                    popup.ShowDialog();
                    if (popup.DialogResult == true)
                    {
                        try
                        {
                            string query = "delete from resources ed where( resources_id = " + dr["resources_id"] + ");";
                            MySqlCommand cmd = new MySqlCommand(query, con);
                            cmd.Connection = con;
                            dt = new DataTable();
                            MySqlDataAdapter sda = new MySqlDataAdapter(cmd);
                            sda.Fill(dt);
                            LoadResourcesData();
                            CheckBox.IsChecked = false;
                        }
                        catch (Exception ex)
                        {
                            VerPopup(ex.Message, "OK");
                        }
                    }
                }
            }
        }

        private void search_box_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (con.State != ConnectionState.Open) { con.Open(); }
            try
            {
                string query = "select *  from resources where( item_Name like '%" + search_box.Text + "%' );";
                MySqlCommand cmd = new MySqlCommand(query, con);
                cmd.Connection = con;
                dt = new DataTable();
                MySqlDataAdapter sda = new MySqlDataAdapter(cmd);
                sda.Fill(dt);
                DataColumn dc = new DataColumn("ischecked");
                dc.DataType = typeof(bool);
                dc.AllowDBNull = false;
                dc.DefaultValue = false;
                dt.Columns.Add(dc);
                resourcestable.ItemsSource = dt.DefaultView;
                CheckBox.IsChecked = false;
                con.Close();
            }
            catch (Exception ex)
            {
                VerPopup(ex.Message, "OK");
            }
        }

        private bool CheckCount()
        {
            int count = 0;
            foreach (DataRow dr in dt.Rows)
            {
                if (Convert.ToBoolean(dr["ischecked"]) == true) { count++; }
            }
            if (count != 1) { return false; }
            else { return true; }
        }

        private void add_button_Click(object sender, RoutedEventArgs e)
        {
            ResourceAddWindow ARwindow = new ResourceAddWindow();
            ARwindow.Height = 600;
            ARwindow.Width = 500;
            ARwindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            ARwindow.ShowDialog();
        }

        private void DataResourceToUpdateFill(out string resource_name, out int quantity, out string designation, out string justification, out string degree, out string status, out DateTime approved_date, out int mr_number, out int po_number, out string payment_status, out string created_by, out string notification)
        {
            resource_name = "";
            quantity = -1;
            designation = "";
            justification = "";
            degree = "";
            status = "";
            approved_date = DateTime.Now;
            mr_number = -1;
            po_number = -1;
            payment_status = "";
            created_by = "";
            notification = "";
            if (CheckCount())
            {
                foreach (DataRow dr in dt.Rows)
                {
                    if (Convert.ToBoolean(dr["ischecked"]))
                    {
                        resource_name = (string)dr["item_name"];
                        quantity = (int)dr["quantity"];
                        designation = (string)dr["designation"];
                        justification = (string)dr["justification"];
                        degree = (string)dr["degree"];
                        status = (string)dr["status"];
                        approved_date = (DateTime)dr["approved_date"];
                        mr_number = (int)dr["Mr_number"];
                        po_number = (int)dr["po_number"];
                        payment_status = (string)dr["payment_status"];
                        created_by = (string)dr["po_created_by"];
                        notification = (string)dr["notification"];
                    }
                }
            }
        }

        private void edit_button_Click(object sender, RoutedEventArgs e)
        {
            DateTime approved_date;
            string resource_name, designation, justification, degree, status, payment_status, created_by, notification;
            int mr_number, po_number, quantity;

            if (CheckCount())
            {
                foreach (DataRow dr in dt.Rows)
                {
                    if (Convert.ToBoolean(dr["ischecked"]))
                    {
                        DataResourceToUpdateFill(out resource_name, out quantity, out designation, out justification, out degree, out status, out approved_date, out mr_number, out po_number, out payment_status, out created_by, out notification);
                        ResourceEditWindow REwindow = new ResourceEditWindow();
                        REwindow.Height = 600;
                        REwindow.Width = 500;
                        REwindow.resource_box.Text = resource_name;
                        REwindow.quantity_box.Value = quantity;
                        REwindow.designation_box.Text = designation;
                        REwindow.justification_box.Text = justification;
                        REwindow.degree_combobox.SelectedValue = degree;
                        REwindow.status_combobox.SelectedValue = status;
                        REwindow.approved_date.Text = approved_date.ToString("yyyy/MM/dd");
                        REwindow.MR_number_box.Text = mr_number.ToString();
                        REwindow.PO_number_box.Text = po_number.ToString();
                        REwindow.payment_status_combobox.SelectedValue = payment_status;
                        REwindow.created_by_box.Text = created_by;
                        REwindow.notification_box.Text = notification;
                        REwindow.Resources_id = (int)dr["resources_id"];
                        REwindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                        REwindow.ShowDialog();
                    }
                }
            }
            else
            {
                VerPopup("Check one item only", "OK");
            }
        }

        //private int getUserPermession()
        //{
        //    if (con.State != ConnectionState.Open) { con.Open(); }
        //    try
        //    {
        //        string query = "select permession_type from user where( username = '" + user + "')";
        //        MySqlCommand cmd = new MySqlCommand(query, con);
        //        cmd.Connection = con;
        //        int user_permession = (int)cmd.ExecuteScalar();
        //        con.Close();
        //        return user_permession;

        //    }
        //    catch (Exception ex)
        //    {
        //        VerPopup(ex.Message, "OK");
        //        return 0;
        //    }
        //}
    }
}